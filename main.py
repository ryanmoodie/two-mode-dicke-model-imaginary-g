#!/usr/bin/env python
# coding=utf-8

"""
main.py
File to be executed to run main program, ie. solve for results and save the
data as pickle files in folder 'simulation_data'.
"""

from argparse import ArgumentParser
from os.path import isfile
from time import clock

import phase_finder_regimes
import time_evolver
from auxiliary import write_out, plog, stopwatch, LINE, INDENT, read_file
from configurer import program_configuration, system_parameters
from defaults import regimes, spaces, third_axes

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "25 Jan 2017"

point_space = spaces[0]
phase_diagram_spaces = spaces[3:-1]
modes_spaces = spaces[1:3]
time_space = spaces[-1]

extra_sr_regimes = regimes[2:4]
sr_regime = regimes[-1]


# def allow_overwrite(s, p, name):
#     def check_value(a, b):
#         try:
#             result = all([ea == eb for ea, eb in zip(a, b)])
#         except TypeError:
#             result = a == b
#         return result
#
#     if isfile(f"{s.write_file_start}{name}{s.write_file_end}"):
#         plog(s, f"{INDENT}File already exists.")
#         if s.overwrite:
#             plog(s, f"{INDENT*2}Will overwrite (overwrite is on).")
#             return True
#         else:
#             old_p = read_file(s, name)[0]
#             same = all([check_value(a, b) for a, b in
#                         zip(vars(old_p).values(), vars(p).values())])
#             if not same:
#                 plog(s, f"{INDENT*2}"
#                         f"Old file has different parameters: will overwrite.")
#                 return True
#             else:
#                 plog(s, f"{INDENT*2}Existing file has same parameters, "
#                         f"so will be reused.")
#                 return False
#     else:
#         plog(s, f"{INDENT*2}Creating new file...")
#         return True


def persistent_action(s, p, regime, space, solver, ax3="", num="", **kwargs):
    l = "_l_" if ax3 else ""
    mod = "_" if num else ""
    base_name = f"{regime}_{space}"
    filename = f"{base_name}{mod}{num}"
    name = f"{base_name}{l}{ax3}"
    if regime is sr_regime and space in phase_diagram_spaces + ["omega_a_v_omega_b_inset"]:
        filenames = [filename, f"{extra_sr_regimes[1]}_{space}",
                     f"{extra_sr_regimes[0]}_{space}"]
        names = [name, f"{extra_sr_regimes[1]}_{space}_l_{ax3}",
                 f"{extra_sr_regimes[0]}_{space}_l_{ax3}"]

    plog(s, f"Solving for: '{name}{mod}{num}'...")
    if num:
        data = getattr(solver, name)(s, p, num, **kwargs)
    else:
        data = getattr(solver, name)(s, p, **kwargs)
    plog(s, f"{INDENT}Done solving for '{name}{mod}{num}'.")

    try:
        multiple = len(data) // len(names)
        for (datum, point), filename in zip(data, filenames * multiple):
            current_name = f"{filename}-{ax3}={point}"
            # if allow_overwrite(s, p, current_name):
            write_out(s, p, datum, current_name)
        return None
    except NameError:
        pass

    try:
        for datum, point in data:
            current_name = f"{filename}-{ax3}={point}"
            # if allow_overwrite(s, p, current_name):
            write_out(s, p, datum, current_name)
    except ValueError:
        # if allow_overwrite(s, p, filename):
        write_out(s, p, data, filename)


def instant_action(s, p, regime, space, solver, **kwargs):
    name = f"{regime}_{space}"
    plog(s, f"Solving for: '{name}'...")
    plog(s, getattr(solver, name, **kwargs)(s, p), -1)


def loop_regimes(s, p, space, regimes_, action, axes3="", **kwargs):
    for regime in regimes_:
        if axes3:
            for ax3 in axes3:
                if getattr(s, ax3) and ax3 not in space.split("_"):
                    action(s, p, regime, space, phase_finder_regimes, ax3, **kwargs)
        else:
            action(s, p, regime, space, phase_finder_regimes, **kwargs)


def main():
    start_time = clock()

    parser = ArgumentParser()
    parser.add_argument('-s', '--program_configuration_file', type=str, default='')
    parser.add_argument('-p', '--parameters_file', type=str, default='')
    args = parser.parse_args()

    s = program_configuration(args.program_configuration_file)
    p = system_parameters(args.parameters_file)

    plog(s, "Main program initialising...")

    if getattr(s, point_space):
        loop_regimes(
            s, p, "point",
            [regime for regime in regimes if regime not in extra_sr_regimes],
            instant_action)

    modes = any([getattr(s, space) for space in modes_spaces])
    phases = any([getattr(s, space) for space in phase_diagram_spaces])

    if modes and not phases:
        reduced_regimes = regimes
    elif not modes and phases:
        reduced_regimes = [
            regime for regime in regimes if regime not in extra_sr_regimes]
    elif getattr(s, time_space) or getattr(s, point_space):
        reduced_regimes = []
    elif not modes and not phases:
        pass
    else:
        exit("Please do modes and phase diagrams in separate runs (sorry).")

    for space in modes_spaces:
        if getattr(s, space):
            for num in (1, 2):
                loop_regimes(s, p, space, reduced_regimes, persistent_action, num=num)

    if getattr(s, "generate_inset"):
        print("Only supports omega_a v omega_b currently")
        loop_regimes(s, p, "omega_a_v_omega_b_inset", reduced_regimes, persistent_action, ["gamma", ])

    for space in phase_diagram_spaces:
        if getattr(s, space):
            loop_regimes(s, p, space, reduced_regimes, persistent_action, third_axes)

    if getattr(s, time_space):
        persistent_action(s, p, "evolve", time_space, time_evolver, "omegas")

    plog(s, f"Program complete.\nRuntime: {stopwatch(start_time)} s.\n{LINE}")


if __name__ == "__main__":
    main()
