#!/usr/bin/env python
#  coding=utf-8

"""
export.py
File to be executed to export pickled data as a csv file, readable by
Mathematica, for plotting. Currently for time evolution only.
"""

from csv import writer
from platform import system as operating_system

from auxiliary import read_file
from configurer import program_configuration

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "22 Jul 2017"


def run():
    s = program_configuration()

    name = "evolve_time-omegas=(-19, 1)"

    p, data = read_file(s, name)

    with open(f"exported_data/{name}.csv", "w") as file:
        if operating_system() == "Windows":
            data_write = writer(file, lineterminator="\n")
        else:
            data_write = writer(file)

        for time, variables in data:
            data_write.writerow([time] + list(variables))


if __name__ == "__main__":
    run()
