#!/usr/bin/env python
# coding=utf-8

"""
phase_finder_core.py
Module containing functions to solve the system of equations.
"""

from multiprocessing import Pool
from time import clock

from numpy import reshape, lexsort

from auxiliary import plog, INDENT, stopwatch_pretty, verbose_parameters, \
    show_progress

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "05 Jul 2017"


def check_region(s, calculation, variables_function, var_i_list, var_j_list):
    start_time = clock()
    plog(s, f"{INDENT}Main calculation under way...")

    with Pool(processes=s.number_of_workers) as pool:
        r = pool.starmap_async(
            calculation,
            [element for row in [
                [variables_function(var_i, var_j) for var_j in var_j_list]
                for var_i in var_i_list]
             for element in row])
        show_progress(s, r)
        result = r.get()

    result = reshape(result, (len(var_i_list), len(var_j_list)))

    plog(s, f"\n{INDENT*2}Main calculation done "
            f"({stopwatch_pretty(start_time)}).")

    return result


def find_eigenvalues(d):
    ordering = lexsort(([x.real for x in d.eigenvalues],
                        [x.imag for x in d.eigenvalues]))
    return [d.eigenvalues[i] for i in ordering]


def solve_modes(s, variable_1_list, variables_function):
    with Pool(processes=s.number_of_workers) as pool:
        result = pool.starmap(
            find_eigenvalues, [variables_function(variable_1) for variable_1 in variable_1_list])
    return result


def check_sr(p, d):
    if d.symmetry:
        return 1
    if abs(d.beta_0) > abs(d.alpha_0) + p.zero:
        return 2
    if abs(d.alpha_0) > abs(d.beta_0) + p.zero:
        return 3


def check_point_stable(p, d):
    if d.normal:
        return d.stable

    if d.spin_z_0 is not None:
        if d.stable:
            return check_sr(p, d)

    d.get_sra(p)
    if d.spin_z_0 is not None:
        if d.stable:
            return check_sr(p, d)

    return 0


def check_single_point(p, d):
    stable = check_point_stable(p, d)
    if d.normal:
        verbose_raw = "stable" if stable else "unstable"
    else:
        if not stable:
            verbose_raw = "unstable"
        else:
            kind = {1: "SR", 2: "SR$\\beta$", 3: "SR$\\alpha$"}[stable]
            verbose_raw = f"{kind} stable"

    for keyword, argument in vars(d).items():
        globals()[keyword] = argument

    try:
        alpha_0 = d.alpha_0
        beta_0 = d.beta_0
        spin_plus_0 = d.spin_plus_0
        spin_z_0 = d.spin_z_0
        d.alpha_0_mod_squared = (alpha_0 * alpha_0.conjugate()).real
        d.beta_0_mod_squared = (beta_0 * beta_0.conjugate()).real
    except AttributeError:
        pass

    return f"State is {verbose_raw}.\n{verbose_parameters(d)}"


if __name__ == "__main__":
    exit()
