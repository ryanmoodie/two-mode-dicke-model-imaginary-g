#!/usr/bin/env python
# coding=utf-8

"""
configurer.py
Module to read values from settings (.cfg) file and calculate values to 
populate settings classes.
"""

from configparser import ConfigParser

# maths stuff may not be used explicitly, but required for eval()!
# So no ctrl-o!
from numpy import linspace, sqrt, pi, exp

import defaults

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "01 Feb 2017"


def get_file_path_start(folder, pre_name):
    return f"{folder}/{pre_name}-"


def get_file_path_end(number, suffix):
    return f"-{number}.{suffix}"


def list_fixed(start, stop, num, **kwargs):
    list_step = abs(start - stop) / (num - 1)

    if stop < start:
        list_end_fixed = stop - list_step
    elif stop > start:
        list_end_fixed = stop + list_step

    list_number_fixed = num + 1
    return linspace(start, list_end_fixed, num=list_number_fixed, **kwargs)


class Settings:
    def __init__(self, default_dict, settings_file):
        user = ConfigParser()
        default = ConfigParser()
        user.read(settings_file)
        default.read_dict(default_dict)

        for section in user.sections():
            for key, value in user[section].items():
                setattr(self, key, eval(
                    default[section][key] if value == "" else value,
                    None, vars(self)))


def system_parameters(extra=''):
    p = Settings(defaults.system_defaults,
                 defaults.get_settings_path(
                     f'{defaults.SYSTEM_PARAMETERS_FILE}{extra}'))

    p.spin_total = 0.5 * p.n
    p.kappa_prime = 0.5 * p.kappa

    for parameter, suffix in [(f"omega_{par}", suf) for par, suf in [
        ("a", s) for s in ("", "inset", "modes_1", "modes_2")] + [("b", s) for s in ("", "inset")] +
            [("av", ""), ("diff", "")]] + [("g", s) for s in ("", "modes")] + [("gamma", ""), ]:
        mod = "_" if suffix else ""
        list_func = "linspace" if "modes" in suffix else "list_fixed"
        setattr(p,
                f"{parameter}_list{mod}{suffix}",
                globals()[list_func](
                    getattr(p, f"{parameter}_list_start{mod}{suffix}"),
                    getattr(p, f"{parameter}_list_end{mod}{suffix}"),
                    getattr(p, f"{parameter}_list_length{mod}{suffix}")
                )
                )

    p.time_step = 1 / p.steps_per_microsecond

    p.omega_points = [tuple([round(i * start + (1 - i) * end, 1) for start, end in zip(*p.end_points)])
                      for i in p.interp_points]

    for variable in ("alpha", "beta", "spin_plus", "spin_z"):
        value = getattr(p, variable)
        if not isinstance(value, list):
            setattr(p, variable, [value] * len(p.omega_points))

    p.initial_variables = list(zip(p.alpha, p.beta, p.spin_plus, p.spin_z))

    for para in ("gamma", "g"):
        name = f"{para}_list_plots"
        value = getattr(p, name)
        if not isinstance(value, list):
            setattr(p, name, [value])

    return p


def graph_settings():
    g = Settings(defaults.graph_defaults,
                 defaults.get_settings_path(defaults.GRAPH_SETTINGS_FILE))
    g.regimes = [regime for regime in defaults.regimes if getattr(g, regime)]
    return g


def program_configuration(extra=''):
    s = Settings(defaults.program_defaults, defaults.get_settings_path(
        f'{defaults.PROGRAM_CONFIGURATION_FILE}{extra}'))

    data_file_suffix = "pkl"
    image_file_suffix = "pdf" if s.vector else 'png'
    log_file_suffix = "log"

    graph_folder = "graphs"
    data_folder = "simulation_data"
    log_folder = "log"

    s.write_file_start = \
        get_file_path_start(data_folder, s.write_file_name)
    s.write_file_end = get_file_path_end(s.number, data_file_suffix)

    s.graph_file_start = \
        get_file_path_start(graph_folder, s.graph_file_name)
    s.graph_file_end = get_file_path_end(s.number, image_file_suffix)

    s.read_file_start = \
        get_file_path_start(data_folder, s.read_file_name)
    s.read_file_end = get_file_path_end(s.number, data_file_suffix)

    s.log_file_start = \
        get_file_path_start(log_folder, s.log_file_name)
    s.log_file_end = \
        get_file_path_end(s.number, log_file_suffix)

    return s


if __name__ == "__main__":
    exit()
