#!/usr/bin/env python
# coding=utf-8

"""
phase_finder_spaces.py
Module to define functions which use functions from phase_finder_core.py module
and have parameters filled appropriately for use by phase_finder_regimes.py
module.
"""

from functools import partial

from phase_finder_core import check_single_point, solve_modes, check_region, \
    check_point_stable

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "01 Feb 2017"


def point(p, d):
    return check_single_point(p, d(p, p.omega_a, p.omega_b, p.g, p.gamma))


def solve_modes_g(s, p, d):
    return solve_modes(s, p.g_list_modes,
                       lambda g: (d(p, p.omega_a, p.omega_b, g, p.gamma),))


def solve_modes_omega(s, p, d, num=""):
    mod = "_" if num else ""
    return solve_modes(s, getattr(p, f"omega_a_list_modes{mod}{num}"),
                       lambda omega_a: (
                           d(p, omega_a, eval(getattr(p, f"omega_b_modes_equals{mod}{num}")), p.g, p.gamma),))


def check_region_omega_a_v_g(s, p, calculation, variables_function):
    return check_region(s, calculation, variables_function,
                        p.omega_a_list, p.g_list)


def check_region_omega_a_v_gamma(s, p, calculation, variables_function):
    return check_region(s, calculation, variables_function,
                        p.omega_a_list, p.gamma_list)


def check_region_omega_a_v_omega_b(s, p, calculation, variables_function):
    return check_region(s, calculation, variables_function,
                        p.omega_a_list, p.omega_b_list)


def check_region_omega_a_v_omega_b_inset(s, p, calculation, variables_function):
    return check_region(s, calculation, variables_function,
                        p.omega_a_list_inset, p.omega_b_list_inset)


def check_region_omega_av_v_omega_diff(s, p, calculation, variables_function):
    return check_region(s, calculation, variables_function,
                        p.omega_av_list, p.omega_diff_list)


def solve_state_stability_omega_a_v_g(s, p, d, gamma):
    return check_region_omega_a_v_g(
        s, p, partial(check_point_stable, p),
        lambda omega_a, g: (d(p, omega_a, eval(p.omega_b_equals), g, gamma),))


def solve_state_stability_omega_a_v_gamma(s, p, d, g):
    return check_region_omega_a_v_gamma(
        s, p, partial(check_point_stable, p),
        lambda omega_a, gamma: (
            d(p, omega_a, eval(p.omega_b_equals), g, gamma),))


def solve_state_stability_omega_a_v_omega_b(s, p, d, g, gamma):
    return check_region_omega_a_v_omega_b(
        s, p, partial(check_point_stable, p),
        lambda omega_a, omega_b: (d(p, omega_a, omega_b, g, gamma),))


def solve_state_stability_omega_a_v_omega_b_inset(s, p, d, g, gamma):
    return check_region_omega_a_v_omega_b_inset(
        s, p, partial(check_point_stable, p),
        lambda omega_a, omega_b: (d(p, omega_a, omega_b, g, gamma),))


def solve_state_stability_omega_av_v_omega_diff(s, p, d, g, gamma):
    return check_region_omega_av_v_omega_diff(
        s, p, partial(check_point_stable, p),
        lambda omega_av, omega_diff: (d(p, omega_av, omega_diff, g, gamma),))


if __name__ == "__main__":
    exit()
