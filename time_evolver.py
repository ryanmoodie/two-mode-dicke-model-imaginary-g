#!/usr/bin/env python
# coding=utf-8

"""
time_evolver.py
Module containing functions to solve the time
evolution of the system of equations.
"""

from functools import partial
from multiprocessing import Pool
from time import clock

from scipy.integrate import ode

from auxiliary import plog, stopwatch, show_progress

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "21 Mar 2017"


def equations_of_motion(t, variables, parameters):
    alpha, beta, spin_plus, spin_z = variables
    omega_a, omega_b, omega_0, g, gamma, kappa = parameters

    spin_minus = spin_plus.conjugate()

    xi = (1.j * (alpha.conjugate() + beta)
          + gamma * (alpha - beta.conjugate()))

    half = xi * spin_minus

    alpha_dot = - (1j * omega_a + 0.5 * kappa) * alpha \
                + g * (gamma * spin_plus - 1.j * spin_minus)

    beta_dot = - (1j * omega_b + 0.5 * kappa) * beta \
               + g * (gamma * spin_minus - 1.j * spin_plus)

    spin_plus_dot = 1j * spin_plus * omega_0 - 2 * g * xi * spin_z

    spin_z_dot = g * (half + half.conjugate())

    return [alpha_dot, beta_dot, spin_plus_dot, spin_z_dot]


def time_evolve(initial_variables, initial_time, time_step, final_time, parameters):
    integrator = ode(equations_of_motion)
    integrator.set_integrator("zvode")
    # , rtol=1e-8, atol=1e-8)
    # , method="bdf") # stiff
    # , method="adams") # non-stiff
    integrator.set_initial_value(initial_variables, initial_time)
    integrator.set_f_params(parameters)
    time_evolution = [(initial_time, initial_variables)]

    while integrator.successful() and integrator.t <= final_time:
        t = integrator.t + time_step
        y = integrator.integrate(t)
        time_evolution.append((t, y))

    return time_evolution


def single_time_evolver(p, omega_point, initial_variables):
    omega_a, omega_b = omega_point
    parameters = [omega_a, omega_b, p.omega_0, p.g, p.gamma, p.kappa]

    return (time_evolve(initial_variables, p.start_time, p.time_step,
                        p.end_time, parameters), omega_point)


def evolve_time_l_omegas(s, p):
    start_time = clock()
    plog(s, "Integrating equations of motion to find time evolution "
            "for set of points...")
    with Pool(processes=s.number_of_workers) as pool:
        r = pool.starmap_async(partial(single_time_evolver, p),
                               zip(p.omega_points, p.initial_variables))
        show_progress(s, r)
        time_evolutions = r.get()
    plog(s, f"Integration complete ({stopwatch(start_time)} s).")
    return time_evolutions


if __name__ == "__main__":
    exit()
