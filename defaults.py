#!/usr/bin/env python
# coding=utf-8

"""
defaults.py
Module which stores default settings and can be run to generate settings files.
"""

from configparser import ConfigParser
from multiprocessing import cpu_count
from os.path import isfile

from scipy.constants import golden

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "03 Apr 2017"

SETTINGS_FOLDER = "settings"
SYSTEM_PARAMETERS_FILE = "parameters"
GRAPH_SETTINGS_FILE = "graphing"
PROGRAM_CONFIGURATION_FILE = "program"
SETTINGS_FILE_SUFFIX = "cfg"


def get_settings_path(file):
    return f"{SETTINGS_FOLDER}/{file}.{SETTINGS_FILE_SUFFIX}"


def make_settings_file(defaults, file, comment="", force=False):
    path = get_settings_path(file)
    if not force:
        if isfile(path):
            if input("Warning: will overwrite existing files"
                     " - proceed? (yes) ") != "yes":
                return None

    config = ConfigParser(allow_no_value=True)
    config.add_section("header")
    config.set("header", f"# {file}.{SETTINGS_FILE_SUFFIX}")
    config.set("header", f"# {comment}")
    config.read_dict(defaults)

    with open(path, "w+") as config_file:
        config.write(config_file)


def main():
    force = input("Overwrite all settings files? (yes)") == "yes"
    make_settings_file(program_defaults, PROGRAM_CONFIGURATION_FILE,
                       "File containing settings for program options.", force)
    make_settings_file(system_defaults, SYSTEM_PARAMETERS_FILE,
                       "File containing settings for system parameters.",
                       force)
    make_settings_file(graph_defaults, GRAPH_SETTINGS_FILE,
                       "File containing settings for plotting.", force)


system_defaults = {
    "other_parameters": {
        "n": 1.e5,
        "kappa": 8.1,
        "omega_0": 0.047,
        "phi": 0.,
        "allow_rotating_solution": False,
    },
    "single_point": {
        "omega_a": 40.,
        "omega_b": "omega_a",
        "g": "0.3/sqrt(n)",
        "gamma": 0.,
    },
    "omega_a_list": {
        "omega_a_list_length": 160,
        "omega_a_list_start": -40.,
        "omega_a_list_end": "-omega_a_list_start",
        "omega_b_equals": "'omega_a'",
    },
    "omega_a_list_modes_1": {
        "omega_a_list_length_modes_1": 1000,
        "omega_a_list_start_modes_1": 10,
        "omega_a_list_end_modes_1": 30,
        "omega_b_modes_equals_1": "'-omega_a+40'",
    },
    "omega_a_list_modes_2": {
        "omega_a_list_length_modes_2": "omega_a_list_length_modes_1",
        "omega_a_list_start_modes_2": "omega_a_list_start_modes_1",
        "omega_a_list_end_modes_2": "omega_a_list_end_modes_1",
        "omega_b_modes_equals_2": "omega_b_modes_equals_1",
    },
    "omega_a_list_inset": {
        "omega_a_list_length_inset": "omega_a_list_length//4",
        "omega_a_list_start_inset": -5,
        "omega_a_list_end_inset": "-omega_a_list_start_inset",
    },
    "omega_b_list_inset": {
        "omega_b_list_length_inset": "omega_a_list_length_inset",
        "omega_b_list_start_inset": "omega_a_list_start_inset",
        "omega_b_list_end_inset": "omega_a_list_end_inset",
    },
    "omega_b_list": {
        "omega_b_list_length": "omega_a_list_length",
        "omega_b_list_start": "omega_a_list_start",
        "omega_b_list_end": "-omega_b_list_start",
    },
    "g_list": {
        "g_list_start": 1.e-4,
        "g_list_end": "1/sqrt(n)",
        "g_list_length": "omega_a_list_length",
        "g_list_plots": "[g]",
    },
    "g_list_modes": {
        "g_list_length_modes": "omega_a_list_length_modes",
        "g_list_start_modes": "g_list_start",
        "g_list_end_modes": "g_list_end",
    },
    "gamma_list": {
        "gamma_list_start": 0.,
        "gamma_list_end": 1.,
        "gamma_list_length": "omega_a_list_length",
        "gamma_list_plots": "[gamma]",
    },
    "omega_av_list": {
        "omega_av_list_length": "omega_a_list_length",
        "omega_av_list_start": "omega_a_list_start/2",
        "omega_av_list_end": "-omega_av_list_start",
    },
    "omega_diff_list": {
        "omega_diff_list_length": "omega_av_list_length",
        "omega_diff_list_start": "omega_av_list_start",
        "omega_diff_list_end": "-omega_diff_list_start",
    },
    "steady_state_finder": {
        "zero": 1e-10,
    },
    "time_evolver": {
        "start_time": 0.,
        "end_time": 5.,
        "steps_per_microsecond": 4,
        "end_points": "[(omega_a, omega_b), (omega_a, omega_b)]",
        "interp_points": [1],
    },
    "initial_varaibles": {
        "alpha": 0.,
        "beta": 0.,
        "spin_plus": 0.,
        "spin_z": "-0.5 * n",
    }
}

graph_defaults = {
    "figures": {
        "multipanel_columns": 1,
        "whole_page_width": False,
    },
    "time_evolver": {
        "plot_bloch_sphere": False,
        "alt_sphere": False,
        "view_angle": [-60, 30],
        "bloch_legend": False,
        "plot_against_time": False,
        "plot_phi": False,
        "plot_real_imag": False,
        "plot_spins": False,
        "use_log_scale": False,
        "legend_location": "'best'",
        "microseconds_from_end_to_show": 35,
    },
    "colourmaps": {
        "x_axis_g_root_n": False,
        "x_axis_khz": False,
        "colourbar": False,
        "limit_cycle_plot_dots": False,
        "spectrum_plot_line": False,
        "plot_u1_symmetry": False,
        "labels": False,
        "title": False,
        "hatches": False,
        "hatch_density": 4,
        "hatch_linewdith": 0.3,
        "inset": False,
        "inset_x": 0.22,
        "inset_y": 0.6,
        "inset_side": 0.25,
    },
    "modes": {
        "tools": False,
        "plot_separate": False,
        "plot_real": False,
        "draw": [5, 6, 7],
        "normal": False,
        "inverted": False,
        "sra": False,
        "srb": False,
        "sr": False,
    }
}

program_defaults = {
    "general_settings": {
        "verbose_level": 0,
        "logging": False,
        "log_level": 1,
        "save_figures": False,
        "show_figures": True,
        "vector": False,
        "combine_spaces": False,
        "generate_inset": False,
        "print_parameters": False,
        "number_of_workers": cpu_count(),
    },
    "space": {
        "point": False,
        "modes_g": False,
        "modes_omega": False,
        "omega_a_v_g": False,
        "omega_a_v_omega_b": False,
        "omega_a_v_gamma": False,
        "omega_av_v_omega_diff": False,
        "time": False,
    },
    "third_axis": {
        "g": False,
        "gamma": False,
    },
    "file_names": {
        "overwrite": False,
        "write_file_name": "'test'",
        "number": 0,
        "read_file_name": "write_file_name",
        "graph_file_name": "read_file_name",
        "log_file_name": "'log'",
    }
}

regimes = ["normal", "inverted", "sra", "srb", "sr"]
spaces = [space for space in program_defaults['space']]
third_axes = [axis for axis in program_defaults['third_axis']]

if __name__ == "__main__":
    main()
