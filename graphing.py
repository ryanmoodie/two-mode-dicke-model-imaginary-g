#!/usr/bin/env python
# coding=utf-8

"""
Graphs.py
Module containing functions to plot graphs of results.
"""


from cmath import log
from itertools import combinations

#import matplotlib as plt
from matplotlib import cm as mcm
from matplotlib import rcParams
from matplotlib.colorbar import make_axes
from matplotlib.colors import ListedColormap
from matplotlib.pyplot import subplots, tight_layout, plot, axes, rc
from numpy import empty, linspace, empty_like, meshgrid, pi, cos, sin, ceil, ma, transpose, sqrt, delete
from pylab import cm
from qutip import Bloch
from scipy.constants import golden

from configurer import graph_settings
from defaults import regimes as defaults_regimes

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "01 Feb 2017"

g = graph_settings()

OMEGA = "$\\omega$"
OMEGA_A = "$\\omega_a$"
OMEGA_B = "$\\omega_b$"
OMEGA_AV = "$\\omega_\\mathrm{av}$"
OMEGA_DIFF = "$\\omega_\\mathrm{diff}$"
G = "$g$"
GAMMA = "$\\gamma$"
PHI = "$\\phi$"
S_X = "$S_x$"
S_Y = "$S_y$"
S_Z = "$S_z$"

NORMAL = '$\\Downarrow$'
INVERTED = '$\\Uparrow$'
SR = 'SR'
SRA = 'SR$\\alpha$'
SRB = 'SR$\\beta$'
LC = 'LC'

REGIMES = {"sra": SRA, "srb": SRB, "sr": SR, "normal": NORMAL, "inverted": INVERTED}

GOLDEN_RATIO = 1 / golden

JOURNAL_COLUMN_WIDTH = 3.375
FUDGE_FACTOR = 1.0
EFFECTIVE_COLUMN_WIDTH = JOURNAL_COLUMN_WIDTH * FUDGE_FACTOR


rc('text', usetex=True)
rcParams.update({"font.size": 10, "font.serif": ['computer roman modern'],
                 "hatch.linewidth": g.hatch_linewidth,'legend.fontsize': 8, 'legend.labelspacing':0.25})


def colour_cycler(i):
    wheel = ('k', 'r', 'b', 'g', 'c', 'm', 'y')
    return wheel[i % len(wheel)]


def style_cycler(i):
    wheel = ('-', '--', '-.', ':')
    return wheel[i % len(wheel)]


class OneDimensionalPlots:
    def __init__(self):
        self.width = 2 * EFFECTIVE_COLUMN_WIDTH if g.whole_page_width else EFFECTIVE_COLUMN_WIDTH

    def one_dimensional_plot(self, x_label, y_labels, number_of_rows=1, number_of_columns=1,
                             sub_aspect=1, slim_dim=False, diff_first=False, share_x_axis_title=False):
        aspect = 2 *sub_aspect * number_of_rows / number_of_columns
        # todo sort out proper width (as commented in email)
        width = self.width * 1.0 #(1 if all([x == 1 for x in (number_of_columns, number_of_rows,)]) else 2)
        number = number_of_rows * number_of_columns
        fig, axss = subplots(nrows=number_of_rows, ncols=number_of_columns, figsize=(width, aspect * width))

        for line in (number_of_rows, number_of_columns):
            if line == 1:
                axss = [axss]

        if not any([isinstance(y_labels, t) for t in (list, tuple,)]):
            y_labels = [y_labels] * number

        i = 0
        for row in axss:
            for ax in row[1 if diff_first else 0:]:
                ax.set_ylabel(y_labels[i],fontsize=8,labelpad=0)
                i += 1
                if i == number or not share_x_axis_title:
                    ax.set_xlabel(x_label,fontsize=8,labelpad=0)
                ax.tick_params(axis='both', which='major', labelsize=8)

        if slim_dim:
            for line in (number_of_rows, number_of_columns):
                if line == 1:
                    [axss] = axss

        return fig, axss

    def plot_single_modes_pane(self, axs, datas, x_list, show_legend=True):
        comps = ("real", "imag") if g.plot_real else ("imag",)
        syms = ("Re", "Im") if g.plot_real else ("Im",)

        edge = 0
        for [data], regime in zip(datas, defaults_regimes):
            if regime in g.regimes:
                edge_top = max([max([max([
                    mode.imag, mode.real]) if g.plot_real else abs(mode.imag)
                                     for i, mode in enumerate(point, start=1)
                                     if i in g.draw]) for point in data])
                edge_bottom = min([min([min([
                    mode.imag, mode.real]) if g.plot_real else abs(mode.imag)
                                        for i, mode in enumerate(point, start=1)
                                        if i in g.draw]) for point in data])

                new_edge = max([abs(x) for x in [edge_top, edge_bottom]])
                if new_edge > edge:
                    edge = new_edge

        if edge < 1e-2:
            for i in range(len(datas)):
                datas[i][0] = [[1000 * mode for mode in point] for point in datas[i][0]]
            unit = "kHz"
        else:
            unit = "MHz"

        for j, ([data], regime) in enumerate(zip(datas, g.regimes)):
            for i, datum in enumerate(transpose(data)):
                if i + 1 in g.draw:
                    for comp, ax in zip(comps, axs):
                        modes = [getattr(d, comp) for d in datum]
                        ax.plot(x_list, modes, label=REGIMES[regime], color=colour_cycler(j), linestyle=style_cycler(j))

        for ax, comp, sym in zip(axs, comps, syms):
            if show_legend:
                handles, labels = ax.get_legend_handles_labels()
                legends = dict(zip(labels, handles))
                ax.legend(legends.values(), legends.keys(), loc="best")
            ax.set_ylabel(f"{sym}($\\lambda$) ({unit})")
            ax.locator_params(nbins=5, axis='x')
            ax.locator_params(nbins=5, axis='y')

    def plot_modes(self, p, datas, x_lists, x_name):
        cols = 2 if g.plot_real else 1
        rows = len(datas)

        fig, axss = self.one_dimensional_plot(x_name, "$\\lambda_i$", number_of_columns=cols,
                                              number_of_rows=rows, sub_aspect=GOLDEN_RATIO, share_x_axis_title=True)

        if not g.plot_real:
            axss = list(zip(*axss))

        for axs, data, x_list, legend in zip(axss, datas, x_lists, [True] + [False] * (rows - 1)):
            self.plot_single_modes_pane(axs, data, x_list, legend)

        tight_layout()

        return [fig]

    def plot_modes_g(self, p, datas):
        g_list = p.g_list_modes
        if g_list[-1] < 1e-2:
            g_list_edit = [x * 1000 for x in g_list]
            unit = "kHz"
        else:
            g_list_edit = g_list
            unit = "MHz"
        return self.plot_modes(p, datas, [g_list_edit], [f"{G} ({unit})"])

    def plot_modes_omega(self, p, datas):
        return self.plot_modes(p, datas, [getattr(p, f"omega_a_list_modes_{num}") for num in (1, 2)],
                               f"{OMEGA_A} (MHz)")

    def plot_simple_time_evolution(self, p, data):
        light_field_names = ["$\Re(\\alpha)$", "$\Im(\\alpha)$", "$\Re(\\beta)$", "$\Im(\\beta)$"] \
            if g.plot_real_imag else ["${|\\alpha|}^2$", "${|\\beta|}^2$"]

        spin_names = ["$\Re(S_+)$", "$\Im(S_+)$", S_Z] if g.plot_real_imag else [S_X, S_Y, S_Z]

        titles = ["Intensity [A.U.]", ]
        namess = [light_field_names, ]

        if g.plot_spins:
            titles.append("Spin components")
            namess.append(spin_names)

        if g.plot_phi:
            titles.append(PHI)
            namess.append([PHI])

        rows = 1
        columns = sum([g.plot_bloch_sphere, g.plot_against_time, g.plot_spins, g.plot_phi])

        fig, axs = self.one_dimensional_plot('Time ($\\mu$s)', titles, slim_dim=True, diff_first=True,
                                             number_of_rows=rows, number_of_columns=columns, )

        start = - g.microseconds_from_end_to_show * p.steps_per_microsecond

        if g.plot_bloch_sphere:
            axs[0].set_axis_off()
            ax_b = fig.add_subplot(rows, columns, 1, projection="3d")
            ax_b.view_init(*g.view_angle)

            if g.alt_sphere:
                r = 1
                num = 50
                polar = linspace(0., pi, num // 2)
                azimuth = linspace(0., 2. * pi, num)
                polars, azimuths = meshgrid(polar, azimuth)
                x = r * sin(polars) * cos(azimuths)
                y = r * sin(polars) * sin(azimuths)
                z = r * cos(polars)

                ax_b.plot_surface(x, y, z, rstride=1, cstride=1, alpha=0.4, cmap=mcm.Greys_r)

            colours = [
                cm.get_cmap('cool', len(p.omega_points))(i)
                for i in range(len(p.omega_points))]

            b = Bloch(fig=fig, axes=ax_b, view=g.view_angle)
            b.frame_alpha = 0.
            b.frame_width = 0.
            b.point_color = [(0, 0, 0, 0)] + colours
            b.font_size = 12
            b.point_size = [6]

            b.add_points([[0, 0], [0, 0], [-1, 1]])
            b.zlpos = [1.1, -1.1]
            b.xlabel = ["", ""]
            b.ylabel = ["", ""]
            b.zlabel = [INVERTED, NORMAL]

            if g.alt_sphere:
                b.sphere_alpha = 0.

            for i, (time_evolution, point) in enumerate(zip(data, p.omega_points)):
                populations = [x[1] for x in time_evolution]

                spins = [
                    [getattr(population[num], comp) for population in
                     populations] for num, comp in
                    zip([2, 2, 3], ["real", "imag", "real"])]

                coords = [
                    [spin / p.spin_total for spin in spin_comp[start:]]
                    for spin_comp in spins]

                b.add_points(coords, meth='l')
                ax_b.plot([], [], label=point, color=colours[i])

            if g.bloch_legend:
                leg = ax_b.legend(loc='lower left', prop={'size': 8}, ncol=2)
                leg.set_title("($\\omega_a$, $\\omega_b$)=", prop={'size': 8})

            ax_b.annotate("(a)",xy=(0.05,0.93), xycoords='axes fraction')
            b.render(fig, ax_b)
            # pos1 = ax_b.get_position()
            # ax_b.set_position([pos1.x0, pos1.y0, pos1.width, pos1.height])

        if g.plot_against_time:
            which = (len(data) // 2) if len(data) > 1 else 0
            print(f"Plotting against time for point: omega_a, omega_b = {p.omega_points[which]}")
            data = data[which]
            t, populations = [x[start:] for x in [[datum[i] for datum in data] for i in range(2)]]

            light_fields = sum(
                [[[getattr(population[i], comp) for population in populations]
                  for comp in ("real", "imag")] for i in range(2)], []) \
                if g.plot_real_imag else \
                [[abs(population[i]) ** 2 for population in populations]
                 for i in range(2)]

            spins = [
                [getattr(population[num], comp) for population in populations]
                for num, comp in zip([2, 2, 3], ["real", "imag", "real"])]

            spin_pluses = [pop[2] for pop in populations]
            phis = [1.j / 2 * log(spin_minus / spin_plus) for
                    spin_minus, spin_plus in zip(
                    [x.conjugate() for x in spin_pluses], spin_pluses)]
            if any([abs(phi.imag) > p.zero for phi in phis]):
                print("Warning! Complex phi!")
            phis = [phi.real for phi in phis]

            listss = [light_fields, ]

            if g.plot_spins:
                listss.append(spins)

            if g.plot_phi:
                listss.append([phis])

            for lists, names, ax, title in zip(listss, namess, axs[1:], titles):
                ax.get_yaxis().set_ticks([])
                for variable_list, name in zip(lists, names):
                    if g.use_log_scale:
                        ax.semilogy(
                            t, [abs(x) for x in variable_list],
                            label="$|" + name + "|$" if any(
                                [x < 0 for x in variable_list])
                            else name)
                    else:
                        ax.plot(t, variable_list, label=name)

                ax.legend(loc=g.legend_location)
                ax.annotate("(b)", xy=(0.05,0.93), xycoords = "axes fraction")

                if g.plot_spins:
                    axs[2 if g.plot_bloch_sphere else 1].set_ylim(-p.spin_total, p.spin_total)

        tight_layout()
        return [fig]


class TwoDimensionalPlots:
    def __init__(self):
        self.solids_cmap = 'tab20'
        self.solids_colours = cm.get_cmap(self.solids_cmap, 20).colors
        self.hatches_cmap = 'Accent'
        self.hatches_colours = cm.get_cmap(self.hatches_cmap, 4).colors

        self.width = 2 * EFFECTIVE_COLUMN_WIDTH if g.whole_page_width else EFFECTIVE_COLUMN_WIDTH

    @staticmethod
    def fix(ax, x_axis, y_axis, data, func="pcolormesh", **kwargs):
        x_min = x_axis[0]
        x_max = x_axis[-1]
        x_step = x_axis[1] - x_min

        y_start = y_axis[0]
        y_end = y_axis[-1]
        y_step = y_axis[1] - y_start

        new_x_min = x_min - 0.5 * x_step
        new_x_max = x_max - 0.5 * x_step

        new_y_start = y_start - 0.5 * y_step
        new_y_end = y_end - 0.5 * y_step

        x_axis_fixed = linspace(
            new_x_min, new_x_max, len(x_axis), endpoint=True)
        y_axis_fixed = linspace(
            new_y_start, new_y_end, len(y_axis), endpoint=True)

        return getattr(ax, func)(x_axis_fixed, y_axis_fixed, data, rasterized=True, **kwargs)

    def start_multipanel(self, data, title, **kwargs):
        number = len(data)

        columns = g.multipanel_columns \
            if number > g.multipanel_columns else number

        rows = int(ceil(number / columns))

        aspect = rows / columns
        height = aspect * self.width

        fig, axs = subplots(nrows=rows, ncols=columns, figsize=(self.width, height), **kwargs)

        if number == 1:
            axs = [axs]

        if g.multipanel_columns > 1 and rows > 1:
            axs = axs.flatten()

        if g.title:
            fig.suptitle(title)

        return fig, axs, number, columns

    def draw_labels(self, ax, x_label, y_label, third_axis_point):
        if g.labels:
            if x_label == OMEGA_B and y_label == OMEGA_A:
                label_sets = {
                    0.2:
                        [
                            (-25, -7, NORMAL), (25, -5, NORMAL),
                            (10, 25, INVERTED), (5, -30, INVERTED),
                            (-25, 11, SRA), (-12, 25, SRB),
                            (33, 20, SRA), (-23, -36, SRB),
                        ],
                    1:
                        [
                            (-20, -53, SRB), (-53, -20, SRA), (33, -33, LC),
                        ],
                }
            elif x_label == G and y_label == OMEGA:
                label_sets = {
                    0: [(1, 25, NORMAL), (1, -25, INVERTED), (2.6, 10, SR)],
                }
            else:
                label_sets = {}

            try:
                for omega_list, y, state in label_sets[third_axis_point]:
                    ax.text(omega_list, y, state, horizontalalignment="center",
                            verticalalignment="center")
            except KeyError:
                pass

    def auto_units(self, p, x_label, x_axis):
        def hertz(prefix=""):
            return f" ({prefix}Hz)"

        basic_unit = hertz("M")

        x_unit = "" if x_label == GAMMA else basic_unit
        y_unit = basic_unit

        if g.x_axis_g_root_n and g.x_axis_khz:
            exit('Define single x_axis unit!')

        if g.x_axis_g_root_n and x_label == G:
            x_axis = [x * sqrt(p.n) for x in x_axis]

        elif g.x_axis_khz and x_label == G:
            if 1.e-3 <= x_axis[-1] < 1.:
                x_axis = [1.e3 * x for x in x_axis]
                x_unit = hertz("k")
            elif x_axis[-1] < 1.e-3:
                x_axis = [1.e6 * x for x in x_axis]
                x_unit = hertz()

        mod = r'$\sqrt{N}$' if g.x_axis_g_root_n else ''

        return x_unit, mod, x_axis, y_unit

    def draw_colourbar(self, fig, axs, colour_map, component_regimes):
        if g.colourbar:
            c_ax, kw = make_axes([ax for ax in axs])
            cbar = fig.colorbar(colour_map, cax=c_ax, **kw)
            tick_locations = [x + 0.5 for x in range(len(component_regimes))]
            cbar.set_ticks(tick_locations)
            cbar.set_ticklabels(component_regimes)

    def draw_u1_symmetry(self, p, ax, x_label, y_label, gamma, omega_a_list, omega_b_list):
        def test_sr_exists(omega_, gamma_):
            if not omega_:
                return False
            return p.spin_total > abs(
                p.omega_0 * (omega_ ** 2 + p.kappa ** 2 / 4) / (omega_ * 4 * p.g ** 2 * (1 + gamma_ ** 2)))

        if g.plot_u1_symmetry:
            u1_sr_colour = self.hatches_colours[-2]
            u1_linewidth = 2

            if x_label == OMEGA_B and y_label == OMEGA_A:
                omega_a_list_start = omega_a_list[0]
                omega_a_list_end = omega_a_list[-2]
                omega_b_list_start = omega_b_list[0]
                omega_b_list_end = omega_b_list[-2]

                min_omega = max(omega_a_list_start, omega_b_list_start)
                max_omega = min(omega_a_list_end, omega_b_list_end)
                omega_list = linspace(min_omega, max_omega,
                                      num=int(sqrt(p.omega_a_list_length ** 2 + p.omega_b_list_length ** 2)))

                j = 0
                while j < len(omega_list):
                    i = j
                    local_omega_list = []
                    while i < len(omega_list) and test_sr_exists(omega_list[i], gamma):
                        local_omega_list += [omega_list[i]]
                        i += 1
                    if local_omega_list:
                        ax.plot(local_omega_list, local_omega_list,
                                '-', linewidth=u1_linewidth, color=u1_sr_colour)
                    j += i - j + 1

                if gamma == 1.:
                    omega_b_right = linspace(p.kappa ** 2 / (4 * omega_a_list_end), omega_b_list_end,
                                             num=len(omega_b_list) // 2)
                    omega_b_left = linspace(omega_b_list_start, p.kappa ** 2 / (4 * omega_a_list_start),
                                            num=len(omega_b_list) // 2)

                    plot(omega_b_right, p.kappa ** 2 / (4 * omega_b_right),
                         '-', linewidth=u1_linewidth, color=u1_sr_colour)
                    plot(omega_b_left, p.kappa ** 2 / (4 * omega_b_left),
                         '-', linewidth=u1_linewidth, color=u1_sr_colour)

            elif x_label == GAMMA and y_label == OMEGA_A:
                omega = float(p.omega_b_equals)
                gamma_list = delete(p.gamma_list, -1)

                i = 0
                local_gamma_list = []
                while i < len(gamma_list) and test_sr_exists(omega, gamma_list[i]):
                    local_gamma_list += [gamma_list[i]]
                    i += 1
                if local_gamma_list:
                    ax.plot(local_gamma_list, [omega for g in local_gamma_list],
                            '-', linewidth=u1_linewidth, color=u1_sr_colour)

                if p.spin_total > abs(p.omega_0 * (omega ** 2 + p.kappa ** 2 / 4) / (2 * omega * p.g ** 2)):
                    hyperbolic_omega_a = (p.kappa ** 2) / (4 * omega)
                    ax.plot([1], [hyperbolic_omega_a],
                            markersize=4, marker='o', color=u1_sr_colour)

                ax.plot([0, 0], [4.78, 5.39], '-', linewidth=u1_linewidth, color=u1_sr_colour)
                # ^ magic numbers for rotating solutions for specific params,
                # full solution complicated so used numerical solution

    def plot_multipanel_of_third_axis(self, p, data, x_axis, y_axis, x_label, y_label, points, draw_inset=False):
        if draw_inset:
            data, inset_data = data
        else:
            inset_data = [None] * len(data)
        fig, axs, number, columns = self.start_multipanel(
            data, f"Phase diagram in the {y_label}, {x_label}-plane", sharex="col", sharey="row")

        spacing = 0.06
        fig.subplots_adjust(hspace=spacing, wspace=spacing)

        for i, (datum, inset_datum, ax, point) in enumerate(zip(data, inset_data, axs, points)):
            x_unit, mod, x_axis, y_unit = self.auto_units(p, x_label, x_axis)

            if i in range(number - columns, number):
                ax.set_xlabel(f"{x_label}{mod}{x_unit}",labelpad=1)

            if not i % columns:
                ax.set_ylabel(f"{y_label}{y_unit}",labelpad=-6)
            if len(points) != 1:
                top_x = x_axis[-1] * 0.8
                top_y = y_axis[-1] * 0.95
                ax.text(top_x, top_y, f"{GAMMA}={point}",
                        color='black' if g.hatches else 'white',
                        horizontalalignment="right", verticalalignment="top")
                print("Warning: assuming third_axis=gamma")

            colour_map, component_regimes = self.plot_single_phase_diagram(ax, datum, x_axis, y_axis)
            self.draw_u1_symmetry(p, ax, x_label, y_label, point, p.omega_a_list, p.omega_b_list)

            if g.limit_cycle_plot_dots:
                for omega_a, omega_b in p.omega_points:
                    ax.plot(omega_b, omega_a, color="black", marker="o", markersize=2)

            if g.spectrum_plot_line:
                omega_as = p.omega_a_list_modes_1
                omega_bs = [eval(p.omega_b_modes_equals_1) for omega_a in omega_as]
                ax.plot(omega_bs, omega_as, color="black")

            if draw_inset:
                aspect = 1

                inset = axes([g.inset_x, g.inset_y, g.inset_side, aspect * g.inset_side], facecolor="white")
                inset.tick_params(axis='both', which='major', labelsize=8)
                #inset.set_xlabel(f"{x_label}{mod}{x_unit}",labelpad = -2,fontsize=8)
                #inset.set_ylabel(f"{y_label}{y_unit}",labelpad=-12,fontsize=8)
                self.plot_single_phase_diagram(inset, inset_datum, p.omega_b_list_inset, p.omega_a_list_inset)
                self.draw_u1_symmetry(p, inset, x_label, y_label, point, p.omega_a_list_inset, p.omega_b_list_inset)

            self.draw_labels(ax, x_label, y_label, point)

        self.draw_colourbar(fig, axs, colour_map, component_regimes)

        return [fig]

    def plot_multipanel_of_spaces(self, p, data):
        fig, axs, number, columns = self.start_multipanel(
            data, f"Multipanel of phase diagrams taking different cuts of phase space")

        for i, (([datum], space), ax) in enumerate(zip(data, axs)):
            if space == "omega_a_v_g":
                x_label = G
                y_label = OMEGA
                x_axis = p.g_list
                y_axis = p.omega_a_list
                ax.annotate("a)",xy=(.05, .9), xycoords='axes fraction')
            elif space == "omega_a_v_gamma":
                x_label = GAMMA
                y_label = OMEGA_A
                x_axis = p.gamma_list
                y_axis = p.omega_a_list
            elif space == "omega_a_v_omega_b":
                x_label = OMEGA_B
                y_label = OMEGA_A
                x_axis = p.omega_b_list
                y_axis = p.omega_a_list
                ax.annotate("b)", xy=(.05, .9), xycoords='axes fraction')
            elif space == "omega_av_v_omega_diff":
                x_label = OMEGA_DIFF
                y_label = OMEGA_AV
                x_axis = p.omega_diff_list
                y_axis = p.omega_av_list
                ax.annotate("c)", xy=(.05, .9), xycoords='axes fraction')

            x_unit, mod, x_axis, y_unit = self.auto_units(p, x_label, x_axis)
            ax.set_xlabel(f"{x_label}{mod}{x_unit}",fontsize=8,labelpad = 0)
            if space == "omega_av_v_omega_diff":
                ax.set_ylabel(f"{y_label}{y_unit}",fontsize=8,labelpad=4)
            else:
                ax.set_ylabel(f"{y_label}{y_unit}",fontsize=8, labelpad=-8)
            colour_map, component_regimes = self.plot_single_phase_diagram(ax, datum, x_axis, y_axis)
            self.draw_u1_symmetry(p, ax, x_label, y_label, p.gamma_list_plots[0], p.omega_a_list, p.omega_b_list)
            self.draw_labels(ax, x_label, y_label, p.gamma_list_plots[0])

        self.draw_colourbar(fig, axs, colour_map, component_regimes)
        tight_layout(w_pad=0.3)
        return [fig]

    def plot_single_phase_diagram(self, ax, data, x_axis, y_axis):
        def assign(*args):
            n = 0
            for set_len_ in reversed(range(1, num_phases + 1)):
                for combination_ in combinations(args, set_len_):
                    if all(combination_):
                        return n
                    n += 1
            return n

        num_phases = len(data)
        maps, regimes = zip(*data)
        map_all = empty(shape=(len(y_axis), len(x_axis)))

        for i in range(len(y_axis)):
            for j in range(len(x_axis)):
                map_all[i, j] = assign(*[maps[k][i][j] for k in range(num_phases)])

        syms = [NORMAL, INVERTED, SRA, SRB, SR]

        symbols = {defaults_regime: symbol for defaults_regime, symbol in
                   zip(defaults_regimes, syms)}

        regimes_possible = []
        for set_len in reversed(range(1, num_phases + 1)):
            for combination in combinations(regimes, set_len):
                regimes_possible.append(
                    ' + '.join([symbols[state] for state in combination]))
        regimes_possible.append(LC)

        regimes_all = [SRA, SRB, SR, LC] if g.hatches else [
            INVERTED,
            '',
            NORMAL,
            '',
            f'{INVERTED} + {SRA}',
            f'{INVERTED} + {SRB}',
            f'{NORMAL} + {SRA}',
            f'{NORMAL} + {SRB}',
            SRA,
            SRB,
            SR,
            f'{SRA} + {SRB}',
            f'{NORMAL} + {INVERTED} + {SRA}',
            f'{NORMAL} + {SR}',
            f'{NORMAL} + {INVERTED} + {SRB}',
            '',
            f'{INVERTED} + {SR}',
            f'{NORMAL} + {INVERTED}',
            LC,
            f'{NORMAL} + {INVERTED} + {SR}'
        ]

        if g.hatches:
            cmap_all = self.hatches_colours
            cmap_all[-1] = [0., 0., 0., 0.]
        else:
            cmap_all = self.solids_colours

        cmap_dict = {regime: cmap for regime, cmap in
                     zip(regimes_all, cmap_all)}

        regimes_present = []
        cmap_present = []
        map_present = empty_like(map_all)
        j = 0

        colourless = [INVERTED, NORMAL,
                      f'{NORMAL} + {INVERTED}', ]
        coloured = [SRA, SRB, LC]

        for i, regime in enumerate(regimes_possible):
            if i in map_all:
                regimes_present.append(regime)
                if g.hatches:
                    if regime in colourless:
                        cmap_present.append([0., 0., 0., 0.])
                    else:
                        if not any([colour in regime for colour in coloured]):
                            cmap_present.append(cmap_dict[SR])
                        else:
                            for colour in coloured:
                                if colour in regime:
                                    cmap_present.append(cmap_dict[colour])
                else:
                    cmap_present.append(cmap_dict[regime])
                for x in range(len(y_axis)):
                    for y in range(len(x_axis)):
                        if map_all[x, y] == i:
                            map_present[x, y] = j
                j += 1

        cmap = ListedColormap(cmap_present)

        colour_map = self.fix(
            ax, x_axis, y_axis, map_present, func="pcolormesh",
            cmap=cmap, vmin=0, vmax=len(regimes_present))

        if g.hatches:
            for symbol, name, colour in \
                    zip(["\\" * g.hatch_density, "/" * g.hatch_density],
                        [INVERTED, NORMAL], ["red", "blue"]):
                for i, regime in enumerate(regimes_present):
                    if name in regime:
                        rcParams.update({"hatch.color": colour})
                        mask = [[x != i for x in row] for row in map_present]
                        map = ma.array(empty_like(map_present), mask=mask)
                        self.fix(
                            ax, x_axis, y_axis, map, func="pcolor",
                            hatch=symbol, alpha=0)

        return colour_map, regimes_present

    def plot_phase_diagram_of_omega_a_v_g(self, p, data):
        return self.plot_multipanel_of_third_axis(
            p, data, p.g_list, p.omega_a_list, G, OMEGA, p.gamma_list_plots)

    def plot_phase_diagram_of_omega_a_v_gamma(self, p, data):
        return self.plot_multipanel_of_third_axis(
            p, data, p.gamma_list, p.omega_a_list, GAMMA, OMEGA_A,
            p.g_list_plots)

    def plot_phase_diagram_of_omega_a_v_omega_b(self, p, data):
        return self.plot_multipanel_of_third_axis(
            p, data, p.omega_b_list, p.omega_a_list, OMEGA_B, OMEGA_A,
            p.gamma_list_plots, draw_inset=g.inset)

    def plot_phase_diagram_of_omega_av_v_omega_diff(self, p, data):
        return self.plot_multipanel_of_third_axis(
            p, data, p.omega_diff_list, p.omega_av_list, OMEGA_DIFF, OMEGA_AV,
            p.gamma_list_plots)


if __name__ == "__main__":
    exit()
