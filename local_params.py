#!/usr/bin/env python
# coding=utf-8

"""
local_params.py
...
"""

from cmath import exp
from cmath import sqrt as csqrt

from numpy import arccos, array
from scipy.linalg import eigvals

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "26 Jul 2017"


class LocalParams:
    def __init__(self, p, omega_a, omega_b, g, gamma, normal=False,
                 omega_av=None):
        self.gamma = gamma
        self.rotating = (not self.gamma) and p.allow_rotating_solution
        if self.rotating:
            self.omega_av = 0.5 * (omega_a + omega_b) \
                if omega_av is None else omega_av
            self.omega_a = self.omega_av
            self.omega_b = self.omega_av
            self.omega_0 = p.omega_0 - (omega_a - omega_b) / 2
        else:
            self.omega_a = omega_a
            self.omega_b = omega_b
            self.omega_0 = p.omega_0

        self.g = g
        self.normal = normal

        if self.normal:
            self.phi = p.phi
        elif (self.omega_a == self.omega_b) or (not self.gamma):
            self.phi = p.phi
            self.symmetry = True
        else:
            zeta = p.kappa ** 2 - 4 * self.omega_a * self.omega_b

            if (not zeta) and self.gamma == 1.:
                self.phi = p.phi
                self.symmetry = True
            else:
                self.symmetry = False
                denominator = self.gamma * zeta
                numerator = p.kappa * (1 - self.gamma ** 2) * \
                            (self.omega_a + self.omega_b)

                if (abs(numerator) > abs(denominator)) or (not denominator):
                    self.phi = None
                else:
                    self.phi = 0.5 * arccos(numerator / denominator)

        self.spin_z_0 = None
        self.spin_plus_0 = None
        self.spin_minus_0 = None
        self.alpha_0 = None
        self.beta_0 = None
        self.matrix = None
        self.eigenvalues = array([float("nan")] * 7)
        self.stable = None

    def get_sr_spin_z(self, p):
        if (self.phi is None) or (self.omega_b == -self.omega_a):
            return None

        if self.rotating:

            spin_z = -self.omega_0 * (
                self.omega_av ** 2 + p.kappa_prime ** 2) / \
                     (4 * self.omega_av * self.g ** 2)

            if abs(spin_z) >= p.spin_total:
                return None

            self.spin_z_0 = spin_z

        else:

            denominator = 2 * self.g ** 2 * ((((1 + self.gamma ** 2) * self.omega_a - (
                1 - self.gamma ** 2) * 1.j * p.kappa_prime -
                             2 * 1.j * self.gamma * self.omega_a
                             * exp(-2.j * self.phi))
                            / (self.omega_a ** 2 + p.kappa_prime ** 2)) +
                           (((1 + self.gamma ** 2) * self.omega_b + (
                               1 - self.gamma ** 2) * 1.j * p.kappa_prime +
                             2.j * self.gamma * self.omega_b
                             * exp(- 2.j * self.phi))
                            / (self.omega_b ** 2 + p.kappa_prime ** 2)))

            if not denominator:
                return None

            spin_z = -self.omega_0 / denominator

            if (abs(spin_z.imag) > p.zero) or (abs(spin_z.real) >= p.spin_total):
                return None

            self.spin_z_0 = spin_z.real

        self.get_existant_variables(p)

    def get_sra(self, p):
        if self.phi is not None:
            self.phi = -self.phi
            self.get_sr_spin_z(p)

    def get_spin_plus(self, spin_total):
        self.spin_plus_0 = csqrt((spin_total ** 2.)
                                 - (self.spin_z_0 ** 2.)) * exp(1.j * self.phi)

    def get_spin_minus(self):
        self.spin_minus_0 = self.spin_plus_0.conjugate()

    def get_alpha(self, kappa):
        self.alpha_0 = - self.g * ((self.spin_plus_0.conjugate()
                                    + 1.j * self.gamma * self.spin_plus_0) /
                                   (self.omega_a - 0.5j * kappa))

    def get_beta(self, kappa):
        self.beta_0 = - self.g * ((self.spin_plus_0 + 1.j * self.gamma *
                                   self.spin_plus_0.conjugate()) /
                                  (self.omega_b - 0.5j * kappa))

    def get_eigenvalues(self, kappa):
        xi_term = (-1.j * self.gamma * self.g * self.alpha_0
                   + self.g * self.alpha_0.conjugate()
                   + self.g * self.beta_0
                   + 1.j * self.gamma * self.g * self.beta_0.conjugate())

        matrix = \
            [
                [
                    self.omega_a - 0.5j * kappa,
                    0.,
                    0.,
                    0.,
                    1.j * self.gamma * self.g,
                    self.g,
                    0.
                ],
                [
                    0.,
                    -self.omega_a - 0.5j * kappa,
                    0.,
                    0.,
                    -self.g,
                    1.j * self.gamma * self.g,
                    0.
                ],
                [
                    0.,
                    0.,
                    self.omega_b - 0.5j * kappa,
                    0.,
                    self.g,
                    1.j * self.gamma * self.g,
                    0.
                ],
                [
                    0.,
                    0.,
                    0.,
                    -self.omega_b - 0.5j * kappa,
                    1.j * self.gamma * self.g,
                    -self.g,
                    0.
                ],
                [
                    -2.j * self.gamma * self.g * self.spin_z_0,
                    2. * self.g * self.spin_z_0,
                    2. * self.g * self.spin_z_0,
                    2.j * self.gamma * self.g * self.spin_z_0,
                    -self.omega_0,
                    0.,
                    2. * xi_term
                ],
                [
                    -2. * self.g * self.spin_z_0,
                    -2.j * self.gamma * self.g * self.spin_z_0,
                    2.j * self.gamma * self.g * self.spin_z_0,
                    -2. * self.g * self.spin_z_0,
                    0.,
                    self.omega_0,
                    -2. * xi_term.conjugate()
                ],
                [
                    (1.j * self.gamma * self.g * self.spin_minus_0 +
                     self.g * self.spin_plus_0),
                    (-self.g * self.spin_minus_0 +
                     1.j * self.gamma * self.g * self.spin_plus_0),
                    (-self.g * self.spin_minus_0 -
                     1.j * self.gamma * self.g * self.spin_plus_0),
                    (-1.j * self.gamma * self.g * self.spin_minus_0 +
                     self.g * self.spin_plus_0),
                    xi_term.conjugate(),
                    -xi_term,
                    0.
                ]
            ]

        self.eigenvalues = eigvals(matrix, check_finite=False)

    def get_existant_variables(self, p):
        self.get_spin_plus(p.spin_total)
        self.get_spin_minus()
        self.get_alpha(p.kappa)
        self.get_beta(p.kappa)
        self.get_eigenvalues(p.kappa)
        self.get_stability(p.zero)

    def get_stability(self, zero):
        self.stable = max(self.eigenvalues.imag) <= zero

        # def check_sr(self, p):
        #     if abs(self.beta_0) > abs(self.alpha_0) + p.zero:
        #         return 2
        #     if abs(self.alpha_0) > abs(self.beta_0) + p.zero:
        #         return 3
        #     else:
        #         return 1


class LocalParamsRotated(LocalParams):
    def __init__(self, p, omega_av, omega_diff, g, gamma, normal=False):
        self.omega_diff = omega_diff
        omega_a = omega_av + self.omega_diff
        omega_b = omega_av - self.omega_diff
        super().__init__(p, omega_a, omega_b, g, gamma, normal, omega_av)


class LocalParamsRotatedNormal(LocalParamsRotated):
    def __init__(self, p, omega_av, omega_diff, g, gamma):
        super().__init__(p, omega_av, omega_diff, g, gamma, normal=True)
        self.spin_z_0 = -p.spin_total
        self.get_existant_variables(p)


class LocalParamsNormal(LocalParams):
    def __init__(self, p, omega_a, omega_b, g, gamma):
        super().__init__(p, omega_a, omega_b, g, gamma, normal=True)
        self.spin_z_0 = -p.spin_total
        self.get_existant_variables(p)


class LocalParamsRotatedInverted(LocalParamsRotated):
    def __init__(self, p, omega_av, omega_diff, g, gamma):
        super().__init__(p, omega_av, omega_diff, g, gamma, normal=True)
        self.spin_z_0 = p.spin_total
        self.get_existant_variables(p)


class LocalParamsInverted(LocalParams):
    def __init__(self, p, omega_a, omega_b, g, gamma):
        super().__init__(p, omega_a, omega_b, g, gamma, normal=True)
        self.spin_z_0 = p.spin_total
        self.get_existant_variables(p)


class LocalParamsRotatedSR(LocalParamsRotated):
    def __init__(self, p, omega_av, omega_diff, g, gamma):
        super().__init__(p, omega_av, omega_diff, g, gamma)
        self.get_sr_spin_z(p)


class LocalParamsSR(LocalParams):
    def __init__(self, p, omega_a, omega_b, g, gamma):
        super().__init__(p, omega_a, omega_b, g, gamma)
        self.get_sr_spin_z(p)


class LocalParamsSRA(LocalParams):
    def __init__(self, p, omega_a, omega_b, g, gamma):
        super().__init__(p, omega_a, omega_b, g, gamma)
        self.get_sra(p)
