# Simulating continuous symmetry breaking with cold atoms in an optical cavity

* Forked from https://bitbucket.org/ryanmoodie/two-mode-dicke-model-original

* Online repository at: https://bitbucket.org/ryanmoodie/two-mode-dicke-model-imaginary-g

* This program aims to map out the phase diagram of an extended two-mode-cavity Dicke model, with g^{\prime} = i \gamma g.

* Written in Python (3.6+), using packages Scipy, Numpy and Matplotlib. 

### Settings notes ###

* Defaults are in defaults.py. Run this module to (re)generate settings files.  
* all frequencies are in MHz
* all times are in microseconds
