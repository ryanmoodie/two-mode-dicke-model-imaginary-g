#!/usr/bin/env python
# coding=utf-8

"""
phase_finder_regimes.py
Module to define functions which use functions from phase_finder_spaces.py
module and have parameters filled appropriately for each option in main.py.
"""

import phase_finder_spaces
from local_params import LocalParamsRotatedNormal, \
    LocalParamsRotatedInverted, LocalParamsRotatedSR, LocalParamsInverted, \
    LocalParamsNormal, LocalParamsSR, LocalParamsSRA

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "31 Mar 2017"


# Point solvers


def normal_point(s, p):
    return phase_finder_spaces.point(p, LocalParamsNormal)


def inverted_point(s, p):
    return phase_finder_spaces.point(p, LocalParamsInverted)


def sr_point(s, p):
    return phase_finder_spaces.point(p, LocalParamsSR)


# Phase diagrams


def process_sr(sol, point):
    sr = [[x == 1 for x in row] for row in sol]
    srb = [[x == 2 for x in row] for row in sol]
    sra = [[x == 3 for x in row] for row in sol]

    return [(sr, point), (srb, point), (sra, point)]


# omega v g


def inverted_omega_a_v_g_l_gamma(s, p):
    return [(phase_finder_spaces.solve_state_stability_omega_a_v_g(
        s, p, LocalParamsInverted, gamma=gamma), gamma)
        for gamma in p.gamma_list_plots]


def normal_omega_a_v_g_l_gamma(s, p):
    return [(phase_finder_spaces.solve_state_stability_omega_a_v_g(
        s, p, LocalParamsNormal, gamma=gamma), gamma)
        for gamma in p.gamma_list_plots]


def sr_omega_a_v_g_l_gamma(s, p):
    results = []
    for gamma in p.gamma_list_plots:
        sol = phase_finder_spaces.solve_state_stability_omega_a_v_g(
            s, p, LocalParamsSR, gamma=gamma)

        results += process_sr(sol, gamma)

    return results


# omega v gamma


def normal_omega_a_v_gamma_l_g(s, p):
    return [(phase_finder_spaces.solve_state_stability_omega_a_v_gamma(
        s, p, LocalParamsNormal, g=g), g) for g in p.g_list_plots]


def inverted_omega_a_v_gamma_l_g(s, p):
    return [(phase_finder_spaces.solve_state_stability_omega_a_v_gamma(
        s, p, LocalParamsInverted, g=g), g) for g in p.g_list_plots]


def sr_omega_a_v_gamma_l_g(s, p):
    results = []
    for g in p.g_list_plots:
        sol = phase_finder_spaces.solve_state_stability_omega_a_v_gamma(
            s, p, LocalParamsSR, g=g)

        results += process_sr(sol, g)

    return results


# omega a v omega b


def normal_omega_a_v_omega_b(s, p, g, gamma):
    return phase_finder_spaces.solve_state_stability_omega_a_v_omega_b(
        s, p, LocalParamsNormal, g=g, gamma=gamma)


def inverted_omega_a_v_omega_b(s, p, g, gamma):
    return phase_finder_spaces.solve_state_stability_omega_a_v_omega_b(
        s, p, LocalParamsInverted, g=g, gamma=gamma)


def sr_omega_a_v_omega_b(s, p, g, gamma):
    return phase_finder_spaces.solve_state_stability_omega_a_v_omega_b(
        s, p, LocalParamsSR, g=g, gamma=gamma)


def normal_omega_a_v_omega_b_inset(s, p, g, gamma):
    return phase_finder_spaces.solve_state_stability_omega_a_v_omega_b_inset(
        s, p, LocalParamsNormal, g=g, gamma=gamma)


def inverted_omega_a_v_omega_b_inset(s, p, g, gamma):
    return phase_finder_spaces.solve_state_stability_omega_a_v_omega_b_inset(
        s, p, LocalParamsInverted, g=g, gamma=gamma)


def sr_omega_a_v_omega_b_inset(s, p, g, gamma):
    return phase_finder_spaces.solve_state_stability_omega_a_v_omega_b_inset(
        s, p, LocalParamsSR, g=g, gamma=gamma)


def normal_omega_a_v_omega_b_l_g(s, p):
    return [(normal_omega_a_v_omega_b(s, p, g, p.gamma), g)
            for g in p.g_list_plots]


def inverted_omega_a_v_omega_b_l_g(s, p):
    return [(inverted_omega_a_v_omega_b(s, p, g, p.gamma), g)
            for g in p.g_list_plots]


def normal_omega_a_v_omega_b_l_gamma(s, p):
    return [(normal_omega_a_v_omega_b(s, p, p.g, gamma), gamma)
            for gamma in p.gamma_list_plots]


def inverted_omega_a_v_omega_b_l_gamma(s, p):
    return [(inverted_omega_a_v_omega_b(s, p, p.g, gamma), gamma)
            for gamma in p.gamma_list_plots]


def sr_omega_a_v_omega_b_l_g(s, p):
    results = []
    for g in p.g_list_plots:
        sol = sr_omega_a_v_omega_b(s, p, g, p.gamma)

        results += process_sr(sol, g)

    return results


def sr_omega_a_v_omega_b_l_gamma(s, p):
    results = []
    for gamma in p.gamma_list_plots:
        sol = sr_omega_a_v_omega_b(s, p, p.g, gamma)

        results += process_sr(sol, gamma)

    return results


def normal_omega_a_v_omega_b_inset_l_gamma(s, p):
    return [(normal_omega_a_v_omega_b_inset(s, p, p.g, gamma), gamma)
            for gamma in p.gamma_list_plots]


def inverted_omega_a_v_omega_b_inset_l_gamma(s, p):
    return [(inverted_omega_a_v_omega_b_inset(s, p, p.g, gamma), gamma)
            for gamma in p.gamma_list_plots]


def sr_omega_a_v_omega_b_inset_l_gamma(s, p):
    results = []
    for gamma in p.gamma_list_plots:
        sol = sr_omega_a_v_omega_b_inset(s, p, gamma, p.gamma)

        results += process_sr(sol, gamma)

    return results


# omega_av v omega_diff


def normal_omega_av_v_omega_diff(s, p, g, gamma):
    return phase_finder_spaces.solve_state_stability_omega_av_v_omega_diff(
        s, p, LocalParamsRotatedNormal, g=g, gamma=gamma)


def inverted_omega_av_v_omega_diff(s, p, g, gamma):
    return phase_finder_spaces.solve_state_stability_omega_av_v_omega_diff(
        s, p, LocalParamsRotatedInverted, g=g, gamma=gamma)


def sr_omega_av_v_omega_diff(s, p, g, gamma):
    return phase_finder_spaces.solve_state_stability_omega_av_v_omega_diff(
        s, p, LocalParamsRotatedSR, g=g, gamma=gamma)


def normal_omega_av_v_omega_diff_l_g(s, p):
    return [(normal_omega_av_v_omega_diff(s, p, g, p.gamma), g)
            for g in p.g_list_plots]


def inverted_omega_av_v_omega_diff_l_g(s, p):
    return [(inverted_omega_av_v_omega_diff(s, p, g, p.gamma), g)
            for g in p.g_list_plots]


def normal_omega_av_v_omega_diff_l_gamma(s, p):
    return [(normal_omega_av_v_omega_diff(s, p, p.g, gamma), gamma)
            for gamma in p.gamma_list_plots]


def inverted_omega_av_v_omega_diff_l_gamma(s, p):
    return [(inverted_omega_av_v_omega_diff(s, p, p.g, gamma), gamma)
            for gamma in p.gamma_list_plots]


def sr_omega_av_v_omega_diff_l_g(s, p):
    results = []
    for g in p.g_list_plots:
        sol = sr_omega_av_v_omega_diff(s, p, g, p.gamma)
        results += process_sr(sol, g)

    return results


def sr_omega_av_v_omega_diff_l_gamma(s, p):
    results = []
    for gamma in p.gamma_list_plots:
        sol = sr_omega_av_v_omega_diff(s, p, p.g, gamma)
        results += process_sr(sol, gamma)

    return results


# Modes against g


def normal_modes_g(s, p):
    return [phase_finder_spaces.solve_modes_g(s, p, LocalParamsNormal)]


def inverted_modes_g(s, p):
    return [phase_finder_spaces.solve_modes_g(s, p, LocalParamsInverted)]


def sra_modes_g(s, p):
    return [phase_finder_spaces.solve_modes_g(s, p, LocalParamsSRA)]


def srb_modes_g(s, p):
    return [phase_finder_spaces.solve_modes_g(s, p, LocalParamsSR)]


def sr_modes_g(s, p):
    return [phase_finder_spaces.solve_modes_g(s, p, LocalParamsSR)]


# Modes against omega


def inverted_modes_omega(s, p, num=""):
    return [phase_finder_spaces.solve_modes_omega(s, p, LocalParamsInverted, num)]


def normal_modes_omega(s, p, num=""):
    return [phase_finder_spaces.solve_modes_omega(s, p, LocalParamsNormal, num)]


def sra_modes_omega(s, p, num=""):
    return [phase_finder_spaces.solve_modes_omega(s, p, LocalParamsSRA, num)]


def srb_modes_omega(s, p, num=""):
    return [phase_finder_spaces.solve_modes_omega(s, p, LocalParamsSR, num)]


def sr_modes_omega(s, p, num=""):
    return [phase_finder_spaces.solve_modes_omega(s, p, LocalParamsSR, num)]


if __name__ == "__main__":
    exit()
